package com.hendisantika.restfulservice.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringRestfulService
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/07/18
 * Time: 08.00
 * To change this template use File | Settings | File Templates.
 */

@Slf4j
public class UtilBase64Image {
    public static String encoder(String imagePath) {
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            // Reading a Image file from file system
            String base64Image = "";
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
            return base64Image;
        } catch (FileNotFoundException e) {
            log.info("Image not found" + e);
        } catch (IOException ioe) {
            log.info("Exception while reading the Image " + ioe);
        }
        return null;
    }

    public static void decoder(String base64Image, String pathFile) {
        try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
            // Converting a Base64 String into Image byte array
            byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
            imageOutFile.write(imageByteArray);
        } catch (FileNotFoundException e) {
            log.info("Image not found" + e);
        } catch (IOException ioe) {
            log.info("Exception while reading the Image " + ioe);
        }
    }
}

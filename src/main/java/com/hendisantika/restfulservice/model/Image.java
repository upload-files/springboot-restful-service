package com.hendisantika.restfulservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringRestfulService
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/07/18
 * Time: 07.53
 * To change this template use File | Settings | File Templates.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Image {
    private String name;
    private String data;

    @Override
    public String toString() {
        String info = String.format("Image name = %s, data = %s", name, data);
        return info;
    }
}

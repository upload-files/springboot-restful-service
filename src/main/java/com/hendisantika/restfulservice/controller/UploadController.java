package com.hendisantika.restfulservice.controller;

import com.hendisantika.restfulservice.model.Image;
import com.hendisantika.restfulservice.util.UtilBase64Image;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringRestfulService
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/07/18
 * Time: 08.03
 * To change this template use File | Settings | File Templates.
 */

@Slf4j
@RestController
public class UploadController {
    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String post(@RequestBody Image image) {
        log.info("/POST request with " + image.toString());
        // save Image to /tmp/upload/ folder
        String path = "/tmp/upload/" + image.getName();
        UtilBase64Image.decoder(image.getData(), path);
        return "/Post Successful!";
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Image get(@RequestParam("name") String name) {
        log.info(String.format("/GET info: imageName = %s", name));
        String imagePath = "/tmp/upload/" + name;
        String imageBase64 = UtilBase64Image.encoder(imagePath);

        if (imageBase64 != null) {
            Image image = new Image(name, imageBase64);
            return image;
        }
        return null;
    }
}
